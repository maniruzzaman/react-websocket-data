import React from "react";
import "./App.css";
import TestComponent from "./component/TestComponent";
import WebSocketDemo from "./component/WebSocketDemo";

function App() {
  return (
    <div className="App">
      {/* <TestComponent /> */}
      <WebSocketDemo />
    </div>
  );
}

export default App;
