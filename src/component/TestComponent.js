import React from "react";
import * as rxjs from "rxjs";
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";
import { webSocket } from "rxjs/webSocket";
import Axios from "axios";

const subject = webSocket(
  "http://localhost:8000/laravel-websockets/api/989678/statistics/"
);

class TestComponent extends React.Component {
  async componentDidMount() {
    // const observable = new Observable(subscriber => {
    //   subscriber.next(1);
    //   subscriber.next(2);
    //   subscriber.next(3);
    //   setTimeout(() => {
    //     subscriber.next(4);
    //     subscriber.complete();
    //   }, 1000);
    // });

    // console.log("just before subscribe");
    // observable.subscribe({
    //   next(x) {
    //     console.log("got value " + x);
    //   },
    //   error(err) {
    //     console.error("something wrong occurred: " + err);
    //   },
    //   complete() {
    //     console.log("done");
    //   }
    // });
    // console.log("just after subscribe");
    let data = [];
    await Axios.get("https://api.portfolio.devsenv.com/api/v1/portfolios").then(
      res => {
        data = res.data.data;
      }
    );
    console.log("data", data);

    const numbers = of(1, 2, 3);
    const values = map(x => x * x)(numbers).subscribe(v =>
      console.log(`value: ${v}`)
    );
  }

  render() {
    return (
      <div>
        <h2>Testing Rx js</h2>
        <hr />
      </div>
    );
  }
}

export default TestComponent;
