import React from "react";
import Websocket from "react-websocket";
import "./test.css";

class WebSocketDemo extends React.Component {
  state = {
    messageList: []
  };

  handleData = messageData => {
    const data = JSON.parse(messageData);

    let messageList = this.state.messageList;
    if (data.length > 0) {
      messageList.push(data[0]);
      this.setState({
        messageList
      });
    }
  };

  render() {
    return (
      <div>
        <h2>
          iBOS - Real time Messaging
          <mark>
            <strong>{this.state.messageList.length}</strong>
          </mark>
        </h2>
        <hr />
        <ul className="list">
          {this.state.messageList.map((item, index) => (
            <li
              className={index % 2 === 0 ? "list-item-left" : "list-item-right"}
              key={index}
            >
              {item.message}
            </li>
          ))}
        </ul>
        <br />
        <Websocket
          url="ws://127.0.0.1:9999/?topic=testTopic&consumerGroup=group1&offset=1"
          onMessage={this.handleData}
        />
      </div>
    );
  }
}

export default WebSocketDemo;
